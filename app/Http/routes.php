<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
    'as'   => 'index',
    'uses' => 'PagesController@index'
]);

Route::get('/account', [
    'as'   => 'account',
    'uses' => 'PagesController@account'
]);

Route::get('/buy-gift-cards', [
    'as'   => 'buy-gift-cards',
    'uses' => 'PagesController@buyGiftCards'
]);

/*Admin Routes*/

Route::get('/gft-admin', [
    'as'   => 'admin',
    'uses' => 'PagesController@admin'
]);

/*Admin - Categories*/
Route::get('/gft-admin/categories', [
    'as'   => 'categories',
    'uses' => 'CategoriesController@index'
]);

Route::get('/gft-admin/categories/create', [
    'as'   => 'create-category',
    'uses' => 'CategoriesController@create'
]);

Route::get('/gft-admin/categories/{id}', [
    'as'   => 'single-category',
    'uses' => 'CategoriesController@show'
]);

Route::post('/gft-admin/categories','CategoriesController@store');
