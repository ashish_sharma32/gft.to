<?php

namespace App\Http\Controllers;

use App\Category;

use Request;

use App\Http\Requests;

class CategoriesController extends Controller
{
    public function index(){
    	$categories = new Category;
    	return view('admin.categories', ['categories' => $categories->getAll()]);
    }

    public function show($id){
    	$categories = new Category;
    	return $categories->getSingle($id);
    }

    public function create(){
        return view('admin.categories');
    }

    public function store(){
     $categories = new Category;
     $categories->storeCategory();
     return redirect(route('categories'));
    }


}
