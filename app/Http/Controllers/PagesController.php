<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;

use App\Http\Requests;

class PagesController extends Controller
{
    public function index(){
		return view('site.index');
	}

	public function admin(){
		return view('admin.dashboard');
	}

	public function account(){
		return view('site.account');
	}

	public function buyGiftCards(){
		$categories = new Category;
    	return view('site.buy-gift-cards', ['categories' => $categories->getAll()]);
	}
}


