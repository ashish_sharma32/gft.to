<?php

namespace App;

use DB;

use Request;

use App\Http\Requests;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name'];

    public function getAll()
  	{
    $categories = Category::all();
    return $categories;
  	}

  	public function getSingle($id)
  	{
    $categories = Category::find($id);
    return $categories;
  	}

    public function storeCategory()
    {
      $input=Request::get('name');
      $category = new Category;
      $category->name = $input;
      $category->save();
    }

}
