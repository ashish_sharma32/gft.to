<!-- JS -->
        
        <!-- jquery-1.11.3.min js
        ============================================ -->         
        <script src="js/vendor/jquery-1.11.3.min.js"></script>
        
        <!-- bootstrap js
        ============================================ -->         
        <script src="js/bootstrap.min.js"></script>
        
        <!-- owl.carousel.min js
        ============================================ -->       
        <script src="js/owl.carousel.min.js"></script>
        
        <!-- lightbox js
        ============================================ -->       
        <script src="js/lightbox.js"></script>
        
        <!-- mixitup js
        ============================================ -->       
        <script src="js/jquery.mixitup.js"></script>
                   
        <!--Price Slider JS-->
        <script src="js/price-slider.js"></script>
        
        <!-- bar filler js
        ============================================ -->       
        <script src="js/jquery.barfiller.js"></script>
        
        <!-- Scroll Up js
        ============================================ -->         
        <script src="js/jquery.scrollUp.min.js"></script>        
        
        <!-- Elevate Zoom js
        ============================================ -->         
        <script src="js/jquery.elevatezoom.js"></script>    
        
        <!-- Meanmenu js
        ============================================ -->         
        <script src="js/meanmenu.js"></script>
        
        <!-- wow js
        ============================================ -->       
        <script src="js/wow.js"></script> 
        <script>
            new WOW().init();
        </script>
        
        <!-- plugins js
        ============================================ -->         
        <script src="js/plugins.js"></script>
        
        <!-- Nevo Slider js
        ============================================ -->         
        <script type="text/javascript" src="lib/custom-slider/js/jquery.nivo.slider.js"></script>
        <script type="text/javascript" src="lib/custom-slider/home.js"></script>    
        
        <!-- main js
        ============================================ -->           
        <script src="js/main.js"></script>