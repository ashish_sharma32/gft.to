<!-- Bootstrap CSS
============================================ -->      
<link rel="stylesheet" href="css/bootstrap.min.css">

<!-- Nivo slider CSS
============================================ -->
<link rel="stylesheet" type="text/css" href="lib/custom-slider/css/nivo-slider.css" media="screen" />   
<link rel="stylesheet" type="text/css" href="lib/custom-slider/css/preview.css" media="screen" />   

<!-- owl.carousel CSS
============================================ -->      
<link rel="stylesheet" href="css/owl.carousel.css">

<!-- owl.theme CSS
============================================ -->      
<link rel="stylesheet" href="css/owl.theme.css">

<!-- owl.transitions CSS
============================================ -->      
<link rel="stylesheet" href="css/owl.transitions.css">

<!--lightbox CSS-->
<link href="css/lightbox.css" rel="stylesheet">

<!--Jquery Ui CSS-->
<link href="css/jquery-ui.css" rel="stylesheet">

<!--Simple Icon CSS-->
<link href="css/simple-line-icons.css" rel="stylesheet">

<!-- bar filler CSS
============================================ -->      
<link rel="stylesheet" href="css/barfiller.css">

<!-- Meanmenu CSS
============================================ -->          
<link rel="stylesheet" href="css/meanmenu.min.css">

<!-- font-awesome.min CSS
============================================ -->      
<link rel="stylesheet" href="css/font-awesome.min.css">

<!-- animate CSS
============================================ -->         
<link rel="stylesheet" href="css/animate.css">

<!-- normalize CSS
============================================ -->        
<link rel="stylesheet" href="css/normalize.css">

<!-- main CSS
============================================ -->          
<link rel="stylesheet" href="css/main.css">

<!-- style CSS
============================================ -->          
<link rel="stylesheet" href="css/style.css">

<!-- responsive CSS
============================================ -->          
<link rel="stylesheet" href="css/responsive.css">