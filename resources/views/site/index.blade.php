@extends('main')
@section('title')
Gft.To: Buy, Sell and Redeem Gift Cards
@stop
@section('content')
<!-- slider start-->
        <section class="slider-area">
            <div class="preview-2">
                <div id="nivoslider" class="slides">    
                    <img src="img/slider/banner.jpg" alt="" title="#slider-direction-1"  />
                </div>
                <!-- direction 1 -->
                <div id="slider-direction-1" class="t-cn slider-direction slider-one">
                    
                    <div class="layer-1-5">
                        <a class="shop_now">SHOP NOW</a>
                    </div>      
                </div>
            </div>
        </section>
        <!-- slider end-->
        
        <!-- Featured Cards Starts -->
        <section class="latest_blog_news text-center">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="offer_title">
                            <h3>FEATURED GIFT CARDS</h3>
                        </div>
                    </div>
                    <div class="client_owl">
                        <div class="col-md-3">   
                            <div class="single_client_logo">
                                <img src="img/cards/amazon.png" alt="AMAZON">
                                <a href="#"><h4 class="featured-card-title" >AMAZON</h4></a>
                            </div>
                        </div> 
                        <div class="col-md-3">      
                            <div class="single_client_logo">
                                
                                    <img src="img/cards/best-buy.jpg" alt="BEST BUY">
                                    <a href="#"><h4 class="featured-card-title" >BEST BUY</h3></a>
                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="single_client_logo">
                                    
                                    <img src="img/cards/walmart.png" alt="WALMART">
                                    <a href="#"><h4 class="featured-card-title" >WALMART</h3></a>
                               
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="single_client_logo">
                                    <img src="img/cards/target.jpg" alt="TARGET">
                                    <a href="#"><h4 class="featured-card-title" >TARGET</h3></a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="single_client_logo">
                                    <img src="img/cards/iTunes.png" alt="iTunes">
                                    <a href="#"><h4 class="featured-card-title" >iTunes</h3></a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="single_client_logo">
                                    <img src="img/cards/hotels-com.png" alt="TARGET">
                                    <a href="#"><h4 class="featured-card-title" >Hotels.com</h3></a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="single_client_logo">
                                    <img src="img/cards/starbucks.png" alt="STARBUCKS">
                                    <a href="#"><h4 class="featured-card-title" >STARBUCKS</h3></a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="single_client_logo">
                                    <img src="img/cards/the-home-depot.png" alt="THE HOME DEPOT">
                                    <a href="#"><h4 class="featured-card-title" >THE HOME DEPOT</h3></a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="single_client_logo">
                                    <img src="img/cards/whole-foods-market.png" alt="WHOLE FOODS MARKET">
                                    <a href="#"><h4 class="featured-card-title" >WHOLE FOODS MARKET</h3></a>
                            </div>
                        </div>
                        <div class="col-md-3">    
                            <div class="single_client_logo">
                                    <img src="img/cards/ebay.png" alt="eBay">
                                    <a href="#"><h4 class="featured-card-title" >eBay</h3></a>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>           
        </section>            
        <!-- Featured Cards Ends -->
@stop