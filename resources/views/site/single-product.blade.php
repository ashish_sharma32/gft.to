<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if lt IE 7 ]> <html lang="en" class="ie6">    <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="">
    
<!-- Mirrored from demo.posthemes.com/pos_hugeshophtml/huge/shop-detail-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 09 Jun 2016 19:11:19 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Shop Detail 1 || HUGE</title>
        <!-- fevicon -->
        <link rel="shortcut icon" href="img/fa.png">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Favicon
		============================================ -->
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">         

 		<!-- CSS  -->
		
		<!-- Fonts CSS
		============================================ -->  
		<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,700,600' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Lato:400,700,300' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Josefin+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Roboto:500' rel='stylesheet' type='text/css'>   
		
		<!-- Bootstrap CSS
		============================================ -->      
        <link rel="stylesheet" href="css/bootstrap.min.css">
        
        <!-- Nivo slider CSS
		============================================ -->
		<link rel="stylesheet" type="text/css" href="lib/custom-slider/css/nivo-slider.css" media="screen" />	
		<link rel="stylesheet" type="text/css" href="lib/custom-slider/css/preview.css" media="screen" />	
        
		<!-- owl.carousel CSS
		============================================ -->      
        <link rel="stylesheet" href="css/owl.carousel.css">
        
		<!-- owl.theme CSS
		============================================ -->      
        <link rel="stylesheet" href="css/owl.theme.css">
           	
		<!-- owl.transitions CSS
		============================================ -->      
        <link rel="stylesheet" href="css/owl.transitions.css">
         
   	    <!--lightbox CSS-->
        <link href="css/lightbox.css" rel="stylesheet">
        
   	    <!--Jquery Ui CSS-->
        <link href="css/jquery-ui.css" rel="stylesheet">
        
   	    <!--Simple Icon CSS-->
        <link href="css/simple-line-icons.css" rel="stylesheet">
                
		<!-- bar filler CSS
		============================================ -->      
        <link rel="stylesheet" href="css/barfiller.css">
                
        <!-- Meanmenu CSS
		============================================ -->          
        <link rel="stylesheet" href="css/meanmenu.min.css">
        
		<!-- font-awesome.min CSS
		============================================ -->      
        <link rel="stylesheet" href="css/font-awesome.min.css">
        
 		<!-- animate CSS
		============================================ -->         
        <link rel="stylesheet" href="css/animate.css">
        
 		<!-- normalize CSS
		============================================ -->        
        <link rel="stylesheet" href="css/normalize.css">
   
        <!-- main CSS
		============================================ -->          
        <link rel="stylesheet" href="css/main.css">
        
        <!-- style CSS
		============================================ -->          
        <link rel="stylesheet" href="style.css">
        
        <!-- responsive CSS
		============================================ -->          
        <link rel="stylesheet" href="css/responsive.css">
        
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body class="shop-detail">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Add your site or application content here -->
        
        <!-- header start -->
        <header class="header">
            <div id="sticker">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-9 col-xs-12">
                            <div class="logo">
                                <a href="index.html"><img src="img/logo.png" alt="HUGE"></a>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-7 hidden-sm">
                            <div class="mainmenu nav navbar-collapse collapse navbar-right">
                                <nav>
                                    <ul class="navbar-nav" id="nav">
                                        <li class="active"><a href="index.html">home</a>
                                            <ul class="sub-menu">
                                                <li><a href="index-2.html">Home Fashion 2</a></li>
                                                <li><a href="index-3.html">Home Fashion 3</a></li>
                                                <li><a href="index-5.html">Home Fashion 4</a></li>
                                                <li><a href="index-6.html">Home Fashion 5</a></li>
                                                <li><a href="index-7.html">Home Fashion 6</a></li>
                                                <li><a href="index-8.html">Home Fashion 7</a></li>
                                            </ul>	
                                        </li>
                                        <li><a href="shop-left-sidebar.html">shop</a>
                                            <div class="mega_menu">
                                                <span>
                                                    <a class="title" href="shop-left-sidebar.html">shop layouts</a>
                                                    <img src="img/arrow.png" alt="">
                                                    <a href="shop-left-sidebar.html">Sidebar Left</a>
                                                    <a href="shop-right-sidebar.html">Sidebar Right</a>
                                                    <a href="shop-list-left-sidebar.html">List View Left</a>
                                                </span>
                                                <span>
                                                    <a class="title" href="shop-left-sidebar.html">shop layouts</a>
                                                    <img src="img/arrow.png" alt="">
                                                    <a href="shop-list-right-sidebar.html">List View Right</a>
                                                    <a href="shop-detail-1.html">Shop Details 1</a>
                                                    <a href="shop-detail-2.html">Shop Details 2</a>
                                                </span>
                                                <span>
                                                    <a class="title" href="shop-left-sidebar.html">shop layouts</a>
                                                    <img src="img/arrow.png" alt="">
                                                    <a href="shop-fullwidth.html">Full Width</a>
                                                    <a href="shop-fullwidth-2-column.html">Full Width 2 Column</a>
                                                    <a href="shop-fullwidth-3-column.html">Full Width 3 Column</a>
                                                </span>
                                            </div>
                                        </li>
                                        <li><a href="blog-left-sidebar.html">blog</a>
                                            <div class="mega_menu">
                                                <span>
                                                    <a class="title" href="blog-left-sidebar.html">blog layouts</a>
                                                    <img src="img/arrow.png" alt="">
                                                    <a href="blog-left-sidebar.html">Left Sidebar</a>
                                                    <a href="blog-right-sidebar.html">Right Sidebar</a>
                                                    <a href="blog-classic-left-sidebar.html">Classic Left Sidebar</a>
                                                </span>
                                                <span>
                                                    <a class="title" href="blog-left-sidebar.html">blog layouts</a>
                                                    <img src="img/arrow.png" alt="">
                                                    <a href="blog-details-left-sidebar.html">Details Left Sidebar</a>
                                                    <a href="blog-details-right-sidebar.html">Details Right Sidebar</a>
                                                    <a href="blog-classic-right-sidebar.html">Classic Right Sidebar</a>
                                                </span>
                                                <span>
                                                    <a href="blog-left-sidebar.html" class="title">blog formats</a>
                                                    <img src="img/arrow.png" alt="">
                                                    <a href="blog-image-format.html">Image format</a>
                                                    <a href="blog-gallery-format.html">Gallery format</a>
                                                    <a href="blog-video-format.html">Video format</a>
                                                </span>
                                            </div>
                                        </li>
                                        <li><a href="about_us.html">pages</a>
                                            <ul class="sub-menu blog">
                                                <li><a href="about_us.html">about us</a></li>
                                                <li><a href="account.html">account</a></li>
                                                <li><a href="cart.html">cart</a></li>
                                                <li><a href="checkout.html">checkout</a></li>
                                                <li><a href="compare.html">compare</a></li>
                                                <li><a href="wishlist.html">wishlist</a></li>
                                                <li><a href="404.html">404 error</a></li>
                                                <li><a href="contact.html">contact us</a></li>
                                            </ul>	
                                        </li>
                                        <li><a href="portfolio-4-col.html">portfolio</a>
                                            <ul class="sub-menu blog">
                                                <li><a href="portfolio-2-col.html">Portfolio two columns</a></li>
                                                <li><a href="portfolio-3-col.html">Portfolio three columns</a></li>
                                                <li><a href="portfolio-4-col.html">Portfolio four columns</a></li>
                                                <li><a href="portfolio-details.html">single portfolio</a></li>
                                            </ul>	
                                        </li>
                                        <li><a href="contact.html">contact</a></li>
                                    </ul>
                                </nav> 
                           </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-md-offset-0 col-sm-3 hidden-xs">
                            <div class="search_cart_sub_menu text-right">
                                <ul>
                                    <li class="search_menu">
                                        <img id="toggle-search" src="img/header_search.png" alt="search">
                                    </li>
                                    <li class="header_cart">
                                        <div class="cart-total">
                                            <ul>
                                                <li>
                                                   <div class="shopping-cart">
                                                       <a href="cart.html"><img src="img/header_cart.png" alt="shopping_cart">
                                                        <p>3</p></a>
                                                   </div>
                                                    <div class="mini-cart-content">
                                                        <div class="cart-img-details">											
                                                            <div class="cart-img-photo">
                                                                <a href="cart.html"><img src="img/trend/trend_acc1.jpg" alt="accessories" /></a>
                                                            </div>
                                                            <div class="cart-img-contaent">
                                                                <a href="#"><h4>Vestibulum suscipit</h4></a>
                                                                <span>1 x $333.00</span>
                                                            </div>
                                                            <div class="pro-del"><a href="#"><i class="fa fa-times-circle"></i></a></div>
                                                        </div>
                                                        <div class="clear"></div>
                                                        <div class="cart-img-details">											
                                                            <div class="cart-img-photo">
                                                                <a href="#"><img src="img/trend/trend_acc2.jpg" alt="accessories" /></a>
                                                            </div>
                                                            <div class="cart-img-contaent">
                                                                <a href="#"><h4>Donec sodales mauris</h4></a>
                                                                <span>1 x $155.00</span>
                                                            </div>
                                                            <div class="pro-del"><a href="#"><i class="fa fa-times-circle"></i></a></div>
                                                        </div>
                                                        <div class="cart-img-details">											
                                                            <div class="cart-img-photo">
                                                                <a href="#"><img src="img/trend/trend_acc3.jpg" alt="accessories" /></a>
                                                            </div>
                                                            <div class="cart-img-contaent">
                                                                <a href="#"><h4>Donec sodales mauris</h4></a>
                                                                <span>1 x $250.00</span>
                                                            </div>
                                                            <div class="pro-del"><a href="#"><i class="fa fa-times-circle"></i></a></div>
                                                        </div>
                                                        <div class="cart-inner-bottom">
                                                            <h5 class="total">Subtotal:.00</h5>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="drop active mini_bar"><a href="#"><i class="fa fa-bars"></i></a>
                                        <ul class="sub-menu">
                                            <li><a href="contact.html">Contact</a></li>
                                            <li><a href="account.html">My Account</a></li>
                                            <li><a href="wishlist.html">Wishlist</a></li>
                                            <li><a href="cart.html">Shopping Cart</a></li>
                                            <li><a href="checkout.html">Checkout</a></li>
                                            <li><a class="mini_bar_title" href="cart.html">Currency</a></li>
                                            <li><a href="cart.html">USD</a></li>
                                            <li><a href="cart.html">EUR</a></li>
                                            <li><a class="mini_bar_title" href="cart.html">Language</a></li>
                                            <li class="flag"><a href="cart.html"><img src="img/flag/de_de.gif" alt=""></a></li>
                                            <li class="flag"><a href="cart.html"><img src="img/flag/english.jpg" alt=""></a></li>
                                            <li class="flag"><a href="cart.html"><img src="img/flag/itali.gif" alt=""></a></li>
                                        </ul>	
                                    </li>
                                </ul>
                            </div>
                        </div>   
                    </div>
                </div>
                <div class="search">
                    <div class="container">
                         <div class="row">
                            <div class="col-sm-6 col-sm-offset-6 col-xs-8 col-xs-offset-4">
                                <div class="search-form">
                                    <form id="search-form"  action="#">
                                        <input type="search" placeholder="Search product..." name="s" />
                                        <input class="fa fa-search" type="submit" value="" />
                                    </form>                                
                                </div>
                            </div>
                        </div>                    
                    </div>
                </div>
            </div>    
            <!-- Mobile Menu Area start -->
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul>
                                        <li><a href="index.html">HOME</a>
                                            <ul>
                                                <li><a href="index-2.html">Home Fashion 2</a></li>
                                                <li><a href="index-3.html">Home Fashion 3</a></li>
                                                <li><a href="index-5.html">Home Fashion 4</a></li>
                                                <li><a href="index-6.html">Home Fashion 5</a></li>
                                                <li><a href="index-7.html">Home Fashion 6</a></li>
                                                <li><a href="index-8.html">Home Fashion 7</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="shop-right-sidebar.html">SHOP</a>
                                            <ul>
                                                <li><a href="shop-left-sidebar.html">Sidebar Left</a></li>
                                                <li><a href="shop-right-sidebar.html">Sidebar Right</a></li>
                                                <li><a href="shop-list-left-sidebar.html">List View Left</a></li>
                                                <li><a href="shop-list-right-sidebar.html">List View Right</a></li>
                                                <li><a href="shop-detail-1.html">Shop Details 1</a></li>
                                                <li><a href="shop-detail-2.html">Shop Details 2</a></li>
                                                <li><a href="shop-fullwidth.html">Full Width</a></li>
                                                <li><a href="shop-fullwidth-2-column.html">Full Width 2 Column</a></li>
                                                <li><a href="shop-fullwidth-3-column.html">Full Width 3 Column</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="blog-left-sidebar.html">BLOG</a>
                                            <ul>
                                                <li><a href="blog-left-sidebar.html">Left Sidebar</a></li>
                                                <li><a href="blog-right-sidebar.html">Right Sidebar</a></li>
                                                <li><a href="blog-classic-left-sidebar.html">Classic Left Sidebar</a></li>
                                                <li><a href="blog-details-left-sidebar.html">Details Left Sidebar</a></li>
                                                <li><a href="blog-details-right-sidebar.html">Details Right Sidebar</a></li>
                                                <li><a href="blog-classic-right-sidebar.html">Classic Right Sidebar</a></li>
                                                <li><a href="blog-image-format.html">Image format</a></li>
                                                <li><a href="blog-gallery-format.html">Gallery format</a></li>
                                                <li><a href="blog-video-format.html">Video format</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="about_us.html">pages</a>
                                            <ul>
                                                <li><a href="about_us.html">about us</a></li>
                                                <li><a href="account.html">account</a></li>
                                                <li><a href="cart.html">cart</a></li>
                                                <li><a href="checkout.html">checkout</a></li>
                                                <li><a href="compare.html">compare</a></li>
                                                <li><a href="wishlist.html">wishlist</a></li>
                                                <li><a href="404.html">404 error</a></li>
                                                <li><a href="contact.html">contact us</a></li>
                                            </ul>	
                                        </li>
                                        <li><a href="portfolio-4-col.html">portfolio</a>
                                            <ul>
                                                <li><a href="portfolio-2-col.html">Portfolio two columns</a></li>
                                                <li><a href="portfolio-3-col.html">Portfolio three columns</a></li>
                                                <li><a href="portfolio-4-col.html">Portfolio four columns</a></li>
                                                <li><a href="portfolio-details.html">single portfolio</a></li>
                                            </ul>	
                                        </li>
                                        <li><a href="contact.html">contact</a></li>
                                    </ul>
                                </nav>
                            </div>					
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu Area end -->
        </header>
        <!-- header end -->
        <!-- banner start -->
        <div class="blog_banner_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page_title">
                            <h2>Carphatt a T-shirt </h2>
                            <div class="breadcrumbs text-center">
                                <ul>
                                    <li><a href="index.html">HOME</a></li>
                                    <li><a href="shop-fullwidth.html">SHOP</a></li>
                                    <li>Carphatt a T-shirt</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- banner end -->
        <!--Start Product Details area  -->
        <section class="product_detail_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="product_img_tab clearfix">
                           <div class="product_main_img tab-content">
                               <div id="one" role="tabpanel"  class="p_tab_img active">
                                   <a href="#"><img id="zoom_01" src="img/shop/shop_detail.jpg" data-zoom-image="img/shop/shop_detail.jpg" alt=""></a>
                               </div>
                               <div id="two" role="tabpanel"  class="p_tab_img">
                                   <a href="#"><img id="zoom_02"  src="img/shop/two_col_3.jpg" data-zoom-image="img/shop/two_col_3.jpg" alt=""></a>
                               </div>
                               <div id="three" role="tabpanel"  class="p_tab_img">
                                   <a href="#"><img id="zoom_03"  src="img/shop/two_col_2.jpg" data-zoom-image="img/shop/two_col_2.jpg"  alt=""></a>
                               </div>
                               <div id="four" role="tabpanel"  class="p_tab_img">
                                   <a href="#"><img id="zoom_04"  src="img/shop/two_col_5.jpg" data-zoom-image="img/shop/two_col_5.jpg"  alt=""></a>
                               </div>
                               <div id="five" role="tabpanel"  class="p_tab_img">
                                   <a href="#"><img id="zoom_05"  src="img/shop/two_col_1.jpg" data-zoom-image="img/shop/two_col_1.jpg"  alt=""></a>
                               </div>
                           </div>
                           <div class="product_img_list">
                               <ul role="tablist">
                                   <li role="presentation" class="active"><a href="#one" aria-controls="one" role="tab" data-toggle="tab"><img src="img/shop/shop_detail.jpg" alt=""></a></li>
                                   <li role="presentation"><a href="#two" aria-controls="two" role="tab" data-toggle="tab"><img src="img/shop/two_col_3.jpg" alt=""></a></li>
                                   <li role="presentation"><a href="#three" aria-controls="three" role="tab" data-toggle="tab"><img src="img/shop/two_col_2.jpg" alt=""></a></li>
                                   <li role="presentation"><a href="#four" aria-controls="three" role="tab" data-toggle="tab"><img src="img/shop/two_col_5.jpg" alt=""></a></li>
                                   <li role="presentation"><a href="#five" aria-controls="three" role="tab" data-toggle="tab"><img src="img/shop/two_col_1.jpg" alt=""></a></li>
                               </ul>
                           </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="product_detail">
                            <div class="product_title">
                                <h2>Carphatt a T-shirt </h2><br>
                            </div>       
                            <p>
                            <a href="#"><i class="fa fa-star"></i></a>
                            <a href="#"><i class="fa fa-star"></i></a>
                            <a href="#"><i class="fa fa-star"></i></a>
                            <a href="#"><i class="fa fa-star"></i></a>
                            <a href="#"><i class="fa fa-star"></i></a>
                            ADD YOUR REVIEW</p>
                            <p><i class="fa fa-check"></i> IN STOCK </p>
                            <p>PRODUCT CODE: <span>SKU-STY3141071-CARP</span></p>
                            <h4>
                                $89.00
                            </h4>
                            <p class="detail">A new style here at Superette, this long sleeve is a hybrid of two always popular styles, so it’s bound to be a winner! similar shape through the body to the Original Neck LS Tee, this has the addition of a raw patch pocket on the chest and doesn’t have Bassike’s signature tail. In an easy to wear grey marl, this is great for layering under a jumper, or even put a thermal under the tee if the cold winter is getting to you!</p>
                            <div class="wish_icon_hover text-center">
                                <ul>
                                    <li class="active cart"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart">ADD TO CART</a></li>
                                    <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                    <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                </ul>
                            </div>     
                            <div class="widget_color">
                               <h4 class="follow">SHARE WITH:</h4>
                               <ul>
                                   <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                   <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                   <li class="youtube"><a href="#"><i class="fa fa-youtube"></i></a></li>
                                   <li class="instagram"><a href="#"><i class="fa fa-instagram"></i></a></li>
                                   <li class="dribbble"><a href="#"><i class="fa fa-dribbble"></i></a></li>
                               </ul>
                           </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="product_description_tab">
                            <div class="description_tab_menu">
                                <ul class="clearfix" role="tablist">
                                    <li role="presentation" class="active"><a href="#description" aria-controls="description" role="tab" data-toggle="tab">Product Description</a></li>
                                    <li role="presentation"><a href="#specification" aria-controls="specification" role="tab" data-toggle="tab">information</a></li>
                                    <li role="presentation"><a href="#review" aria-controls="review" role="tab" data-toggle="tab">customer reviews (6)</a></li>
                                    <li role="presentation"><a href="#tags" aria-controls="tags" role="tab" data-toggle="tab">product tags</a></li>
                              </ul>
                            </div>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="description">
                                   <p>A new style here at Superette, this long sleeve is a hybrid of two always popular styles, so it’s bound to be a winner! A similar shape through the body to the Original Neck LS Tee, this has the addition of a raw patch pocket on the chest and doesn’t have Bassike’s signature tail. In an easy to wear grey marl, this is great for layering under a jumper, or even put a thermal under the tee if the cold winter is getting to you!</p>
                                    <ul>
                                        <li> <i class="fa fa-circle"></i>  Slim fit long sleeve t-shirt with raw neckline  </li>
                                        <li> <i class="fa fa-circle"></i> Raw edge patch pocket on chest</li>
                                        <li> <i class="fa fa-circle"></i> Raw sleeve and body finish with Bassike signature back neck chainstich</li>
                                        <li> <i class="fa fa-circle"></i>  100% organic cotton superfine jersey</li>
                                        <li> <i class="fa fa-circle"></i> Machine washable in laundry bag</li>
                                        <li> <i class="fa fa-circle"></i> Preshrunk so you can tumble dry</li>
                                        <li> <i class="fa fa-circle"></i> Made in Australia</li>
                                    </ul>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="specification">
                                    <p>Veniam quasi voluptatem facere nesciunt laborum, quibusdam amet totam fugit, blanditiis doloribus alias eveniet dolore pariatur dolores aliquid!</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex consectetur minima quod officiis magni, aspernatur. Ea consectetur ab in, consequatur alias, quo sit. Optio vitae cupiditate, consectetur veritatis cumque odio magnam voluptates voluptas eligendi, minima tenetur voluptatum dolor autem, doloribus expedita obcaecati.</p>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="review">
                                    <p>Similique animi consequatur pariatur voluptas tempore, dolores obcaecati dolorum quia odit harum. Quos nemo, minima totam quidem ipsum labore.</p>
                                    <ul>
                                        <li> <i class="fa fa-circle"></i> Minus placeat eligendi neque doloribus sed ratione repellendus a illo similique, sint quisquam perferendis eum nam nihil dolor fugit blanditiis, explicabo, recusandae hic qui exercitationem aspernatur excepturi voluptate unde. </li>
                                        <li> <i class="fa fa-circle"></i> Quaerat magnam, perferendis, sapiente doloremque error omnis esse in saepe quos eveniet quasi ex fugit eligendi consectetur nobis amet. </li>
                                    </ul>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="tags">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,    quis  rud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore. ncididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
                                    <ul>
                                        <li> <i class="fa fa-circle"></i> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis  </li>
                                        <li> <i class="fa fa-circle"></i> Distinctio cum architecto asperiores ut, tempora amet quidem ullam! Tenetur debitis sequi dolorum consequuntur quae explicabo sapiente laboriosam! Hic, unde.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </section>
        <!--End Product Details area  -->
        <!-- related start -->
        <section class="featured_area section_padding_7">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section_title section_padding_3 text-center">
                            <h2>RELATED ITEMS</h2>
                            <img src="img/section_title.png" alt="">
                        </div>
                        <div class="featured_owl_wrapper">
                            <div class="col-xs-12">
                                <div class="single_trend">
                                    <div class="single_trend_item">
                                        <div class="single_trend_img">
                                            <a href="#">
                                                <img class="primary_image" src="img/trend/trend_acc1.jpg" alt="">
                                                <img class="secondary_image" src="img/trend/trend_acc2.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="single_trend_label text-center">    
                                            <a href="#"><h3>Vinperl handbag</h3></a>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <h4>&pound;190.00</h4>
                                        </div>
                                        <div class="wish_icon_hover text-center">
                                            <ul>
                                                <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                <li><a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-bar-chart"></i></a></li>
                                                <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">    
                                <div class="single_trend">
                                    <div class="single_trend_item">
                                        <div class="single_trend_img">
                                            <a href="#">
                                                <img class="primary_image" src="img/trend/trend_bag2.jpg" alt="">
                                                <img class="secondary_image" src="img/trend/trend_bag2.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="single_trend_label text-center">    
                                            <a href="#"><h3>Vinperl handbag</h3></a>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <h4>&pound;190.00</h4>
                                        </div>
                                        <div class="wish_icon_hover text-center">
                                            <ul>
                                                <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                <li><a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-bar-chart"></i></a></li>
                                                <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="col-xs-12">
                                <div class="single_trend">
                                    <div class="single_trend_item">
                                        <div class="single_trend_img">
                                            <a href="#">
                                                <img class="primary_image" src="img/trend/trend_man2.jpg" alt="">
                                                <img class="secondary_image" src="img/trend/trend_man1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="single_trend_label text-center">    
                                            <a href="#"><h3>Vinperl handbag</h3></a>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <h4>&pound;190.00</h4>
                                        </div>
                                        <div class="wish_icon_hover text-center">
                                            <ul>
                                                <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                <li><a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-bar-chart"></i></a></li>
                                                <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="col-xs-12">   
                                <div class="single_trend">
                                    <div class="single_trend_item">
                                        <div class="single_trend_img">
                                            <a href="#">
                                                <img class="primary_image" src="img/trend/trend_bag3.jpg" alt="">
                                                <img class="secondary_image" src="img/trend/trend_bag4.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="single_trend_label text-center">    
                                            <a href="#"><h3>Vinperl handbag</h3></a>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <h4>&pound;190.00</h4>
                                        </div>
                                        <div class="wish_icon_hover text-center">
                                            <ul>
                                                <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                <li><a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-bar-chart"></i></a></li>
                                                <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">    
                                <div class="single_trend">
                                    <div class="single_trend_item">
                                        <div class="single_trend_img">
                                            <a href="#">
                                                <img class="primary_image" src="img/trend/trend_women2.jpg" alt="">
                                                <img class="secondary_image" src="img/trend/trend_women1.jpg" alt="">
                                            </a>
                                        </div>
                            <div class="single_trend_label text-center">    
                                            <a href="#"><h3>Vinperl handbag</h3></a>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <h4>&pound;190.00</h4>
                                        </div>
                                        <div class="wish_icon_hover text-center">
                                            <ul>
                                                <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                <li><a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-bar-chart"></i></a></li>
                                                <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">    
                                <div class="single_trend">
                                    <div class="single_trend_item">
                                        <div class="single_trend_img">
                                            <a href="#">
                                                <img class="primary_image" src="img/trend/trend_acc1.jpg" alt="">
                                                <img class="secondary_image" src="img/trend/trend_acc2.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="single_trend_label text-center">    
                                            <a href="#"><h3>Vinperl handbag</h3></a>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <h4>&pound;190.00</h4>
                                        </div>
                                        <div class="wish_icon_hover text-center">
                                            <ul>
                                                <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                <li><a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-bar-chart"></i></a></li>
                                                <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">    
                                <div class="single_trend">
                                    <div class="single_trend_item">
                                        <div class="single_trend_img">
                                            <a href="#">
                                                <img class="primary_image" src="img/trend/trend_bag2.jpg" alt="">
                                                <img class="secondary_image" src="img/trend/trend_bag2.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="single_trend_label text-center">    
                                            <a href="#"><h3>Vinperl handbag</h3></a>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <h4>&pound;190.00</h4>
                                        </div>
                                        <div class="wish_icon_hover text-center">
                                            <ul>
                                                <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                <li><a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-bar-chart"></i></a></li>
                                                <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">    
                                <div class="single_trend">
                                    <div class="single_trend_item">
                                        <div class="single_trend_img">
                                            <a href="#">
                                                <img class="primary_image" src="img/trend/trend_man2.jpg" alt="">
                                                <img class="secondary_image" src="img/trend/trend_man1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="single_trend_label text-center">    
                                            <a href="#"><h3>Vinperl handbag</h3></a>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <h4>&pound;190.00</h4>
                                        </div>
                                        <div class="wish_icon_hover text-center">
                                            <ul>
                                                <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                <li><a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-bar-chart"></i></a></li>
                                                <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="col-xs-12">       
                                <div class="single_trend">
                                    <div class="single_trend_item">
                                        <div class="single_trend_img">
                                            <a href="#">
                                                <img class="primary_image" src="img/trend/trend_bag3.jpg" alt="">
                                                <img class="secondary_image" src="img/trend/trend_bag4.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="single_trend_label text-center">    
                                            <a href="#"><h3>Vinperl handbag</h3></a>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <h4>&pound;190.00</h4>
                                        </div>
                                        <div class="wish_icon_hover text-center">
                                            <ul>
                                                <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                <li><a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-bar-chart"></i></a></li>
                                                <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">    
                                <div class="single_trend">
                                    <div class="single_trend_item">
                                        <div class="single_trend_img">
                                            <a href="#">
                                                <img class="primary_image" src="img/trend/trend_women2.jpg" alt="">
                                                <img class="secondary_image" src="img/trend/trend_women1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="single_trend_label text-center">    
                                            <a href="#"><h3>Vinperl handbag</h3></a>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <h4>&pound;190.00</h4>
                                        </div>
                                        <div class="wish_icon_hover text-center">
                                            <ul>
                                                <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                <li><a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-bar-chart"></i></a></li>
                                                <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- related end -->
        <!-- trending start -->
        <section class="trending">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="section_title text-center section_padding_3">
                            <h2>customer who viewed this items also viewed</h2>
                            <img src="img/section_title.png" alt="">
                        </div> 
                        <div class="clerfix"></div>
                        <div class="trend_content row">
                            <div id="arrival" role="tabpanel" class="active trend_item">
                                <div class="trend_item_slider">
                                    <div class="col-xs-12">  
                                        <div class="single_trend">
                                            <div class="single_trend_item">
                                                <div class="single_trend_img">
                                                    <a href="#">
                                                        <img class="primary_image" src="img/trend/trend_bag1.jpg" alt="">
                                                        <img class="secondary_image" src="img/trend/trend_bag2.jpg" alt="">
                                                        <span class="blue">-15%</span>
                                                    </a>
                                                </div>
                                                <div class="single_trend_label text-center">    
                                                    <a href="#"><h3>Vinperl handbag</h3></a>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <h4>&pound;190.00 <del>$260.00</del></h4>
                                                </div>
                                                <div class="wish_icon_hover text-center">
                                                    <ul>
                                                        <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-bar-chart"></i></a></li>
                                                        <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="single_trend">
                                            <div class="single_trend_item">
                                                <div class="single_trend_img">
                                                    <a href="#">
                                                        <img class="primary_image" src="img/trend/trend_man1.jpg" alt="">
                                                        <img class="secondary_image" src="img/trend/trend_man2.jpg" alt="">
                                                    </a>
                                                </div>
                                                <div class="single_trend_label text-center">    
                                                    <a href="#"><h3>Pearl braey Classic Polo</h3></a>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <h4>&pound;120.00</h4>
                                                </div>
                                                <div class="wish_icon_hover text-center">
                                                    <ul>
                                                        <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-bar-chart"></i></a></li>
                                                        <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="single_trend">
                                            <div class="single_trend_item">
                                                <div class="single_trend_img">
                                                    <a href="#">
                                                        <img class="primary_image" src="img/trend/trend_man3.jpg" alt="">
                                                        <img class="secondary_image" src="img/trend/trend_man4.jpg" alt="">
                                                        <span class="purple">HOT</span>
                                                    </a>
                                                </div>
                                                <div class="single_trend_label text-center">    
                                                    <a href="#"><h3>Victoria  Draey Coats</h3></a>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <h4>&pound;190.00</h4>
                                                </div>
                                                <div class="wish_icon_hover text-center">
                                                    <ul>
                                                        <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-bar-chart"></i></a></li>
                                                        <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="single_trend">
                                            <div class="single_trend_item">
                                                <div class="single_trend_img">
                                                    <a href="#">
                                                        <img class="primary_image" src="img/trend/trend_women1.jpg" alt="">
                                                        <img class="secondary_image" src="img/trend/trend_women2.jpg" alt="">
                                                    </a>
                                                </div>
                                                <div class="single_trend_label text-center">    
                                                    <a href="#"><h3>Draey trend dress</h3></a>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <h4>&pound;310.00</h4>
                                                </div>
                                                <div class="wish_icon_hover text-center">
                                                    <ul>
                                                        <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-bar-chart"></i></a></li>
                                                        <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">  
                                        <div class="single_trend">
                                            <div class="single_trend_item">
                                                <div class="single_trend_img">
                                                    <a href="#">
                                                        <img class="primary_image" src="img/trend/trend_bag1.jpg" alt="">
                                                        <img class="secondary_image" src="img/trend/trend_bag2.jpg" alt="">
                                                        <span class="blue">-15%</span>
                                                    </a>
                                                </div>
                                                <div class="single_trend_label text-center">    
                                                    <a href="#"><h3>Vinperl handbag</h3></a>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <h4>&pound;190.00 <del>$260.00</del></h4>
                                                </div>
                                                <div class="wish_icon_hover text-center">
                                                    <ul>
                                                        <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-bar-chart"></i></a></li>
                                                        <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="single_trend">
                                            <div class="single_trend_item">
                                                <div class="single_trend_img">
                                                    <a href="#">
                                                        <img class="primary_image" src="img/trend/trend_man1.jpg" alt="">
                                                        <img class="secondary_image" src="img/trend/trend_man2.jpg" alt="">
                                                    </a>
                                                </div>
                                                <div class="single_trend_label text-center">    
                                                    <a href="#"><h3>Pearl braey Classic Polo</h3></a>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <h4>&pound;120.00</h4>
                                                </div>
                                                <div class="wish_icon_hover text-center">
                                                    <ul>
                                                        <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-bar-chart"></i></a></li>
                                                        <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="single_trend">
                                            <div class="single_trend_item">
                                                <div class="single_trend_img">
                                                    <a href="#">
                                                        <img class="primary_image" src="img/trend/trend_man3.jpg" alt="">
                                                        <img class="secondary_image" src="img/trend/trend_man4.jpg" alt="">
                                                        <span class="purple">HOT</span>
                                                    </a>
                                                </div>
                                                <div class="single_trend_label text-center">    
                                                    <a href="#"><h3>Victoria  Draey Coats</h3></a>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <h4>&pound;190.00</h4>
                                                </div>
                                                <div class="wish_icon_hover text-center">
                                                    <ul>
                                                        <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-bar-chart"></i></a></li>
                                                        <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="single_trend">
                                            <div class="single_trend_item">
                                                <div class="single_trend_img">
                                                    <a href="#">
                                                        <img class="primary_image" src="img/trend/trend_women1.jpg" alt="">
                                                        <img class="secondary_image" src="img/trend/trend_women2.jpg" alt="">
                                                    </a>
                                                </div>
                                                <div class="single_trend_label text-center">    
                                                    <a href="#"><h3>Draey trend dress</h3></a>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <h4>&pound;310.00</h4>
                                                </div>
                                                <div class="wish_icon_hover text-center">
                                                    <ul>
                                                        <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-bar-chart"></i></a></li>
                                                        <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                            </div>
                            <div id="men" role="tabpanel" class="trend_item">   
                                <div class="trend_item_slider">
                                    <div class="col-xs-12">
                                        <div class="single_trend">
                                            <div class="single_trend_item">
                                                <div class="single_trend_img">
                                                    <a href="#">
                                                        <img class="primary_image" src="img/trend/trend_man3.jpg" alt="">
                                                        <img class="secondary_image" src="img/trend/trend_man4.jpg" alt="">
                                                        <span class="purple">HOT</span>
                                                    </a>
                                                </div>
                                                <div class="single_trend_label text-center">    
                                                    <a href="#"><h3>Victoria  Draey Coats</h3></a>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <h4>&pound;190.00</h4>
                                                </div>
                                                <div class="wish_icon_hover text-center">
                                                    <ul>
                                                        <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-bar-chart"></i></a></li>
                                                        <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="single_trend">
                                            <div class="single_trend_item">
                                                <div class="single_trend_img">
                                                    <a href="#">
                                                        <img class="primary_image" src="img/trend/trend_man5.jpg" alt="">
                                                        <img class="secondary_image" src="img/trend/trend_man6.jpg" alt="">
                                                        <span class="purple">HOT</span>
                                                    </a>
                                                </div>
                                                <div class="single_trend_label text-center">    
                                                    <a href="#"><h3>Victoria  Draey Coats</h3></a>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <h4>&pound;190.00</h4>
                                                </div>
                                                <div class="wish_icon_hover text-center">
                                                    <ul>
                                                        <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-bar-chart"></i></a></li>
                                                        <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="single_trend">
                                            <div class="single_trend_item">
                                                <div class="single_trend_img">
                                                    <a href="#">
                                                        <img class="primary_image" src="img/trend/trend_man3.jpg" alt="">
                                                        <img class="secondary_image" src="img/trend/trend_man4.jpg" alt="">
                                                        <span class="purple">HOT</span>
                                                    </a>
                                                </div>
                                                <div class="single_trend_label text-center">    
                                                    <a href="#"><h3>Victoria  Draey Coats</h3></a>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <h4>&pound;190.00</h4>
                                                </div>
                                                <div class="wish_icon_hover text-center">
                                                    <ul>
                                                        <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-bar-chart"></i></a></li>
                                                        <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="single_trend">
                                            <div class="single_trend_item">
                                                <div class="single_trend_img">
                                                    <a href="#">
                                                        <img class="primary_image" src="img/trend/trend_man5.jpg" alt="">
                                                        <img class="secondary_image" src="img/trend/trend_man6.jpg" alt="">
                                                        <span class="purple">HOT</span>
                                                    </a>
                                                </div>
                                                <div class="single_trend_label text-center">    
                                                    <a href="#"><h3>Victoria  Draey Coats</h3></a>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <h4>&pound;190.00</h4>
                                                </div>
                                                <div class="wish_icon_hover text-center">
                                                    <ul>
                                                        <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-bar-chart"></i></a></li>
                                                        <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="single_trend">
                                            <div class="single_trend_item">
                                                <div class="single_trend_img">
                                                    <a href="#">
                                                        <img class="primary_image" src="img/trend/trend_man3.jpg" alt="">
                                                        <img class="secondary_image" src="img/trend/trend_man4.jpg" alt="">
                                                        <span class="purple">HOT</span>
                                                    </a>
                                                </div>
                                                <div class="single_trend_label text-center">    
                                                    <a href="#"><h3>Victoria  Draey Coats</h3></a>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <h4>&pound;190.00</h4>
                                                </div>
                                                <div class="wish_icon_hover text-center">
                                                    <ul>
                                                        <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-bar-chart"></i></a></li>
                                                        <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="single_trend">
                                            <div class="single_trend_item">
                                                <div class="single_trend_img">
                                                    <a href="#">
                                                        <img class="primary_image" src="img/trend/trend_man5.jpg" alt="">
                                                        <img class="secondary_image" src="img/trend/trend_man6.jpg" alt="">
                                                        <span class="purple">HOT</span>
                                                    </a>
                                                </div>
                                                <div class="single_trend_label text-center">    
                                                    <a href="#"><h3>Victoria  Draey Coats</h3></a>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <h4>&pound;190.00</h4>
                                                </div>
                                                <div class="wish_icon_hover text-center">
                                                    <ul>
                                                        <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-bar-chart"></i></a></li>
                                                        <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="single_trend">
                                            <div class="single_trend_item">
                                                <div class="single_trend_img">
                                                    <a href="#">
                                                        <img class="primary_image" src="img/trend/trend_man3.jpg" alt="">
                                                        <img class="secondary_image" src="img/trend/trend_man4.jpg" alt="">
                                                        <span class="purple">HOT</span>
                                                    </a>
                                                </div>
                                                <div class="single_trend_label text-center">    
                                                    <a href="#"><h3>Victoria  Draey Coats</h3></a>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <h4>&pound;190.00</h4>
                                                </div>
                                                <div class="wish_icon_hover text-center">
                                                    <ul>
                                                        <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-bar-chart"></i></a></li>
                                                        <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="single_trend">
                                            <div class="single_trend_item">
                                                <div class="single_trend_img">
                                                    <a href="#">
                                                        <img class="primary_image" src="img/trend/trend_man5.jpg" alt="">
                                                        <img class="secondary_image" src="img/trend/trend_man6.jpg" alt="">
                                                        <span class="purple">HOT</span>
                                                    </a>
                                                </div>
                                                <div class="single_trend_label text-center">    
                                                    <a href="#"><h3>Victoria  Draey Coats</h3></a>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <h4>&pound;190.00</h4>
                                                </div>
                                                <div class="wish_icon_hover text-center">
                                                    <ul>
                                                        <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Like it!"><img src="img/trend/heart.png" alt=""></a></li>
                                                        <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-bar-chart"></i></a></li>
                                                        <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>   
                    </div>    
                </div>
            </div>
        </section>
        <!-- trending end -->
        <!-- client start -->
        <div class="client_area">
            <div class="container">
                <div class="row">
                    <div class="client_owl">
                        <div class="col-md-3">   
                            <div class="single_client_logo">
                                <img src="img/client/client_1.jpg" alt="client">
                            </div>
                        </div> 
                        <div class="col-md-3">      
                            <div class="single_client_logo">
                                <img src="img/client/client_2.jpg" alt="client">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="single_client_logo">
                                <img src="img/client/client_3.jpg" alt="client">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="single_client_logo">
                                <img src="img/client/client_4.jpg" alt="client">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="single_client_logo">
                                <img src="img/client/client_5.jpg" alt="client">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="single_client_logo">
                                <img src="img/client/client_1.jpg" alt="client">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="single_client_logo">
                                <img src="img/client/client_2.jpg" alt="client">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="single_client_logo">
                                <img src="img/client/client_3.jpg" alt="client">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="single_client_logo">
                                <img src="img/client/client_4.jpg" alt="client">
                            </div>
                        </div>
                        <div class="col-md-3">    
                            <div class="single_client_logo">
                                <img src="img/client/client_5.jpg" alt="client">
                            </div>
                        </div> 
                    </div>     
                </div>
            </div>
        </div>
        <!-- client end -->
        <!-- purchase start -->
        <div class="purchase_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="purchane_banner">
                            <h4>Do you love <span>hugeshop</span> design? Buy it now & Rate it</h4>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="purchase_button">
                            <a href="#" class="purchase">purchase now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- purchase end -->
        <!-- footer start -->
        <footer class="footer_area">
            <div class="footer_top_area text-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="logo">
                                <a href="index.html"><img src="img/logo_footer.png" alt="logo"></a>
                                <p>Lorem ipsum dolor sit amet consectetur adipiscing elit. Phasellus diam arcu, placerat utodio
ultrices vehicula erat mauris diam egestas nec lacus sit amet.</p>
                                <img class="devider" src="img/footer_devider.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="single_contact">
                                <a href="#"><i class="fa fa-phone"></i></a>
                                <p>(300) 123 456 789 - 888</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">       
                            <div class="single_contact">
                                <a href="#"><i class="fa fa-map-marker"></i></a>
                                <p>65 Huge Street, Melbourne  Victoria<br>
                                3050 Australia.</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">        
                            <div class="single_contact">
                                <a href="#"><i class="fa fa-envelope-o"></i></a>
                                <a href="#"><p>Infor@hugeshop.com</p></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_middle_area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                            <div class="footer_widget">
                                <h4>company</h4>
                                <ul class="footer_menu">
                                    <li><a href="#">about us</a></li>
                                    <li><a href="#">affiliate program</a></li>
                                    <li><a href="#">careers</a></li>
                                    <li><a href="#">press releases</a></li>
                                    <li><a href="#">terms &amp; conditions</a></li>
                                    <li><a href="#">contact us</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 hidden-sm col-xs-12">
                            <div class="footer_widget">
                                <h4>customer service</h4>
                                <ul class="footer_menu">
                                    <li><a href="#">help &amp; faq</a></li>
                                    <li><a href="#">orders &amp; returns</a></li>
                                    <li><a href="#">guarantee</a></li>
                                    <li><a href="#">shipping &amp; policies</a></li>
                                    <li><a href="#">serch &amp; terms</a></li>
                                    <li><a href="#">store locations</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                            <div class="footer_widget">
                                <h4>my account</h4>
                                <ul class="footer_menu">
                                    <li><a href="#">my orders</a></li>
                                    <li><a href="#">my credit slips</a></li>
                                    <li><a href="#">my addresses</a></li>
                                    <li><a href="#">my personal info</a></li>
                                    <li><a href="#">specials</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                            <div class="footer_widget">
                                <h4>newletter</h4>
                                <form action="#">
                                    <input type="text" placeholder="Enter your email address">
                                    <a href="#" class="submit"><i class="fa fa-long-arrow-right"></i></a>
                                </form>
                                <h4 class="follow">follow us on</h4>
                                <ul class="social_icon">
                                    <li class="facebook"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li class="twitter"><a target="_blank" href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li class="dribbble"><a target="_blank" href="#"><i class="fa fa-dribbble"></i></a></li>
                                    <li class="google-plus"><a target="_blank" href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li class="instagram"><a target="_blank" href="#"><i class="fa fa-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_bottom_area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-7 col-xs-12">
                            <div class="copyright">
                                <p>Copyright ©2015 <a target="_blank" href="http://bootexperts.com/"> <span> BootExpert</span></a>. All right Reserved</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-5 col-xs-12">
                            <div class="payment">
                                <a href="#"><img src="img/footer_payment_color.png" alt="payment"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- footer end -->

        <!-- QUICKVIEW PRODUCT -->
        <div id="quickview-wrapper">
            <!-- Modal -->
            <div class="modal fade" id="productModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="modal-product">
                                <div class="product-images">
                                    <div class="main-image images">
                                        <img alt="" src="img/14.jpg">
                                    </div>
                                </div><!-- .product-images -->

                                <div class="product-info">
                                    <h1>Diam quis cursus</h1>
                                    <div class="price-box">
                                        <p class="price"><span class="special-price"><span class="amount">$132.00</span></span></p>
                                    </div>
                                    <a href="shop.html" class="see-all">See all features</a>
                                    <div class="quick-add-to-cart">
                                        <form method="post" class="cart">
                                            <div class="numbers-row">
                                                <input type="number" id="french-hens" value="3">
                                            </div>
                                            <button class="single_add_to_cart_button" type="submit">Add to cart</button>
                                        </form>
                                    </div>
                                    <div class="quick-desc">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, suscipit id nulla.
                                    </div>
                                    <div class="social-sharing">
                                        <div class="widget widget_socialsharing_widget">
                                            <h3 class="widget-title-modal">Share this product</h3>
                                            <ul class="social-icons">
                                                <li><a target="_blank" title="Facebook" href="#" class="facebook social-icon"><i class="fa fa-facebook"></i></a></li>
                                                <li><a target="_blank" title="Twitter" href="#" class="twitter social-icon"><i class="fa fa-twitter"></i></a></li>
                                                <li><a target="_blank" title="Pinterest" href="#" class="pinterest social-icon"><i class="fa fa-pinterest"></i></a></li>
                                                <li><a target="_blank" title="Google +" href="#" class="gplus social-icon"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a target="_blank" title="LinkedIn" href="#" class="linkedin social-icon"><i class="fa fa-linkedin"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div><!-- .product-info -->
                            </div><!-- .modal-product -->
                        </div><!-- .modal-body -->
                    </div><!-- .modal-content -->
                </div><!-- .modal-dialog -->
            </div>
            <!-- END Modal -->
        </div>
        <!-- END QUICKVIEW PRODUCT -->
        
        <!-- JS -->
        
 		<!-- jquery-1.11.3.min js
		============================================ -->         
        <script src="js/vendor/jquery-1.11.3.min.js"></script>
        
 		<!-- bootstrap js
		============================================ -->         
        <script src="js/bootstrap.min.js"></script>
        
   		<!-- lightbox js
		============================================ -->       
        <script src="js/lightbox.js"></script>
        
   		<!-- owl.carousel.min js
		============================================ -->       
        <script src="js/owl.carousel.min.js"></script>
          
        <!-- mixitup js
		============================================ -->       
        <script src="js/jquery.mixitup.js"></script>
                   
        <!--Price Slider JS-->
        <script src="js/price-slider.js"></script>
        
        <!-- bar filler js
		============================================ -->       
        <script src="js/jquery.barfiller.js"></script>
        
        <!-- Scroll Up js
		============================================ -->         
        <script src="js/jquery.scrollUp.min.js"></script>        
        
        <!-- Elevate Zoom js
		============================================ -->         
        <script src="js/jquery.elevatezoom.js"></script>    
        
        <!-- Meanmenu js
		============================================ -->         
        <script src="js/meanmenu.js"></script>
           
        <!-- wow js
        ============================================ -->       
        <script src="js/wow.js"></script> 
        <script>
            new WOW().init();
        </script>
        
   		<!-- plugins js
		============================================ -->         
        <script src="js/plugins.js"></script>
        
   		<!-- Nevo Slider js
		============================================ -->         
		<script type="text/javascript" src="lib/custom-slider/js/jquery.nivo.slider.js"></script>
        <script type="text/javascript" src="lib/custom-slider/home.js"></script>   
        
   		<!-- main js
		============================================ -->           
        <script src="js/main.js"></script>
    </body>

<!-- Mirrored from demo.posthemes.com/pos_hugeshophtml/huge/shop-detail-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 09 Jun 2016 19:11:30 GMT -->
</html>
