@extends('main')
@section('title')
Login or Register | GFT.TO - Buy, Sell and Redeem Gift Cards
@stop
@section('content')
    <!-- banner start -->
        <div class="blog_banner_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page_title">
                            <h2>Login/Register</h2>
                            <div class="breadcrumbs text-center">
                                <ul>
                                    <li><a href="index.html">HOME</a></li>
                                    <li>Login/Register</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- banner end -->
        <!-- register start -->
        <div class="register_area">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="register_customer">
                            <h2 class="register_heading">Registered Customer</h2>
                            <p>You can login with social networks</p>
                            <div class="social_login clearfix">
                                <a class="facebook" href="#"><i class="fa fa-facebook"></i>Sign in with Facebook</a>
                                <a class="twitter" href="#"><i class="fa fa-twitter"></i>Sign in with twitter</a>
                            </div>
                            <form action="#">
                                <p>Email Address<span> * </span><br>
                                    <input type="text">
                                </p>
                                <p>Password<span> * </span><br>
                                    <input type="text">
                                </p>
                                <div class="row">
                                    <p class="col-sm-6 col-xs-12">
                                         <input type="checkbox">Remember Me
                                    </p>
                                    <p class="col-sm-6 col-xs-12">
                                        <a href="#">Forgot your Password?</a>
                                    </p>
                                </div>
                                <input type="submit" value="LOGIN">
                            </form>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="register_customer">
                            <h2 class="register_heading">Creat a new account</h2>
                            <form action="#">
                                <p>Username<span> * </span><br>
                                    <input type="text">
                                </p>
                                <p>Email<span> * </span><br>
                                    <input type="text">
                                </p>
                                <p>Password<span> * </span><br>
                                    <input type="text">
                                </p>
                                <p>Re-Password<span> * </span><br>
                                    <input type="text">
                                </p>
                                <input type="submit" value="REGISTER">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- register end -->
@stop