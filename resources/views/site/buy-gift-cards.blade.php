@extends('main')
@section('title')
Buy Gift Cards | GFT.TO - Buy, Sell and Redeem Gift Cards
@stop
@section('content')
        <!-- banner start -->
        <div class="blog_banner_area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page_title">
                            <h2>BUY GIFT CARDS</h2>
                            <div class="breadcrumbs text-center">
                                <ul>
                                    <li><a href="index.html">HOME</a></li>
                                    <li>BUY GIFT CARDS</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- banner end -->
        <!--Start shop page area  -->
        <section class="shop_page_area section_padding_6">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                         <div class="row">        
                            <div class="search_retailers">
                                <form action="#">
                                    <input class="form-control input-lg" id="inputlg" type="text" placeholder="Search Retailers">
                                </form>
                            </div>
                        </div>
                        <div class="row">    
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="single_trend">
                                    <div class="single_trend_item">
                                        <div class="single_trend_img">
                                            
                                                <img class="primary_image" src="img/cards_shop/amazon.png" alt="">
                                           
                                        </div>
                                        <div class="single_trend_label text-center">    
                                            <a href="#"><h3>AMAZON</h3></a>
                                            <h4><span>&#36;10<span> - <span>&#36;500</span></h4>
                                        </div>
                                        <div class="wish_icon_hover text-center">
                                            <ul>
                                                <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="single_trend">
                                    <div class="single_trend_item">
                                        <div class="single_trend_img">
                                            
                                                <img class="primary_image" src="img/cards_shop/best-buy.jpg" alt="">
                                          
                                        </div>
                                        <div class="single_trend_label text-center">    
                                            <a href="#"><h3>BEST BUY</h3></a>
                                            <h4><span>&#36;10<span> - <span>&#36;500</span></h4>
                                        </div>
                                        <div class="wish_icon_hover text-center">
                                            <ul>
                                                <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="single_trend">
                                    <div class="single_trend_item">
                                        <div class="single_trend_img">
                                            <a href="#">
                                                <img class="primary_image" src="img/cards_shop/walmart.png" alt="">
                                            </a>
                                        </div>
                                        <div class="single_trend_label text-center">    
                                            <a href="#"><h3>WALMART</h3></a>
                                            <h4><span>&#36;10<span> - <span>&#36;500</span></h4>
                                        </div>
                                        <div class="wish_icon_hover text-center">
                                            <ul>
                                                <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="single_trend">
                                    <div class="single_trend_item">
                                        <div class="single_trend_img">
                                            <a href="#">
                                                <img class="primary_image" src="img/cards_shop/target.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="single_trend_label text-center">    
                                            <a href="#"><h3>TARGET</h3></a>
                                            <h4><span>&#36;10<span> - <span>&#36;500</span></h4>
                                        </div>
                                        <div class="wish_icon_hover text-center">
                                            <ul>
                                                <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="single_trend">
                                    <div class="single_trend_item">
                                        <div class="single_trend_img">
                                            <a href="#">
                                                <img class="primary_image" src="img/cards_shop/iTunes.png" alt="">
                                            </a>
                                        </div>
                                        <div class="single_trend_label text-center">    
                                            <a href="#"><h3>iTunes</h3></a>
                                            <h4><span>&#36;10<span> - <span>&#36;500</span></h4>
                                        </div>
                                        <div class="wish_icon_hover text-center">
                                            <ul>
                                                <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="single_trend">
                                    <div class="single_trend_item">
                                        <div class="single_trend_img">
                                            <a href="#">
                                                <img class="primary_image" src="img/cards_shop/hotels-com.png" alt="">
                                            </a>
                                        </div>
                                        <div class="single_trend_label text-center">    
                                            <a href="#"><h3>Hotels.Com</h3></a>
                                            <h4><span>&#36;10<span> - <span>&#36;500</span></h4>
                                        </div>
                                        <div class="wish_icon_hover text-center">
                                            <ul>
                                                <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="single_trend">
                                    <div class="single_trend_item">
                                        <div class="single_trend_img">
                                            <a href="#">
                                                <img class="primary_image" src="img/cards_shop/starbucks.png" alt="">
                                            </a>
                                        </div>
                                        <div class="single_trend_label text-center">    
                                            <a href="#"><h3>STARBUCKS</h3></a>
                                            <h4><span>&#36;10<span> - <span>&#36;500</span></h4>
                                        </div>
                                        <div class="wish_icon_hover text-center">
                                            <ul>
                                                <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="single_trend">
                                    <div class="single_trend_item">
                                        <div class="single_trend_img">
                                            <a href="#">
                                                <img class="primary_image" src="img/cards_shop/the-home-depot.png" alt="">
                                            </a>
                                        </div>
                                        <div class="single_trend_label text-center">    
                                            <a href="#"><h3>THE HOME DEPOT</h3></a>
                                            <h4><span>&#36;10<span> - <span>&#36;500</span></h4>
                                        </div>
                                        <div class="wish_icon_hover text-center">
                                            <ul>
                                                <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="single_trend">
                                    <div class="single_trend_item">
                                        <div class="single_trend_img">
                                            <a href="#">
                                                <img class="primary_image" src="img/cards_shop/whole-foods-market.png" alt="">
                                            </a>
                                        </div>
                                        <div class="single_trend_label text-center">    
                                            <a href="#"><h3>THE WHOLE FOODS MARKET</h3></a>
                                            <h4><span>&#36;10<span> - <span>&#36;500</span></h4>
                                        </div>
                                        <div class="wish_icon_hover text-center">
                                            <ul>
                                                <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="single_trend">
                                    <div class="single_trend_item">
                                        <div class="single_trend_img">
                                            <a href="#">
                                                <img class="primary_image" src="img/cards_shop/ebay.png" alt="">
                                            </a>
                                        </div>
                                        <div class="single_trend_label text-center">    
                                            <a href="#"><h3>eBay</h3></a>
                                            <h4><span>&#36;10<span> - <span>&#36;500</span></h4>
                                        </div>
                                        <div class="wish_icon_hover text-center">
                                            <ul>
                                                <li class="active"><a href="#" data-toggle="tooltip" title="Shopping Cart"><img src="img/trend/shopping_cart.png" alt="cart"></a></li>
                                                <li><a class="modal-view" href="#" data-toggle="modal" data-target="#productModal"><img src="img/trend/eye.png" alt=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>   
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                       <div class="shop_left_sidebar">
                           <div class="widget">
                               <h3 class="widget-title">Buy Gift Cards Online</h3>
                               <div class="widget_categories">
                                   <ul>
                                    @foreach ($categories as $category)
                                        <li><a href="#">{{ $category->name }}</a></li>
                                    @endforeach 
                                    </ul>
                               </div>
                           </div>
                       </div>
                   </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="pagination text-center">
                                <ul class="page_no">
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li class="active"><a href="#"><i class="fa fa-long-arrow-right"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>        
        </section>
        <!--End shop page area  -->                                               

        <!-- QUICKVIEW PRODUCT -->
        <div id="quickview-wrapper">
            <!-- Modal -->
            <div class="modal fade" id="productModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="modal-product">
                                <div class="product-images">
                                    <div class="main-image images">
                                        <img alt="" src="img/14.jpg">
                                    </div>
                                </div><!-- .product-images -->

                                <div class="product-info">
                                    <h1>Diam quis cursus</h1>
                                    <div class="price-box">
                                        <p class="price"><span class="special-price"><span class="amount">$132.00</span></span></p>
                                    </div>
                                    <a href="shop.html" class="see-all">See all features</a>
                                    <div class="quick-add-to-cart">
                                        <form method="post" class="cart">
                                            <div class="numbers-row">
                                                <input type="number" id="french-hens" value="3">
                                            </div>
                                            <button class="single_add_to_cart_button" type="submit">Add to cart</button>
                                        </form>
                                    </div>
                                    <div class="quick-desc">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, suscipit id nulla.
                                    </div>
                                    <div class="social-sharing">
                                        <div class="widget widget_socialsharing_widget">
                                            <h3 class="widget-title-modal">Share this product</h3>
                                            <ul class="social-icons">
                                                <li><a target="_blank" title="Facebook" href="#" class="facebook social-icon"><i class="fa fa-facebook"></i></a></li>
                                                <li><a target="_blank" title="Twitter" href="#" class="twitter social-icon"><i class="fa fa-twitter"></i></a></li>
                                                <li><a target="_blank" title="Pinterest" href="#" class="pinterest social-icon"><i class="fa fa-pinterest"></i></a></li>
                                                <li><a target="_blank" title="Google +" href="#" class="gplus social-icon"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a target="_blank" title="LinkedIn" href="#" class="linkedin social-icon"><i class="fa fa-linkedin"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div><!-- .product-info -->
                            </div><!-- .modal-product -->
                        </div><!-- .modal-body -->
                    </div><!-- .modal-content -->
                </div><!-- .modal-dialog -->
            </div>
            <!-- END Modal -->
        </div>
        <!-- END QUICKVIEW PRODUCT -->
        
@stop