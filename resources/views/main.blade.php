<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if lt IE 7 ]> <html lang="en" class="ie6">    <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="">
    <head>
        @include('partials.meta')
        @include('partials.fonts')
        @include('partials.stylesheet')
        @include('partials.pollyfill')
    </head>
    <body class="home_three">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- header start -->
        <header class="header">
            <div id="sticker">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-8 col-xs-12">
                            <div class="logo">
                                <a href="{{URL::route('index')}}"><img src="img/logo.png" alt="Gft.To"></a>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 hidden-sm">
                            <div class="mainmenu nav navbar-collapse collapse navbar-right">
                                <nav>
                                    <ul class="navbar-nav" id="nav">
                                        <li class="active"><a href="{{URL::route('buy-gift-cards')}}">Buy Gift Cards</a></li>
                                        <li class="active"><a href="{{URL::route('index')}}">Sell Gift Cards</a></li>
                                        <li class="active"><a href="{{URL::route('account')}}">Login</a></li>
                                        <li class="active"><a href="{{URL::route('account')}}">Sign Up Free</a></li>
                                    </ul>
                                </nav> 
                           </div>
                        </div>   
                    </div>
                </div>    
                <!-- Mobile Menu Start -->
                <div class="mobile-menu-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="mobile-menu">
                                    <nav id="dropdown">
                                        <ul>
                                            <li><a href="{{URL::route('buy-gift-cards')}}">BUY GIFT CARDS</a></li>
                                            <li><a href="#">SELL GIFT CARDS</a></li>
                                            <li><a href="{{URL::route('account')}}">LOGIN</a></li>
                                            <li><a href="{{URL::route('account')}}">SIGN UP FREE</a></li>
                                        </ul>
                                    </nav>
                                </div>                  
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Mobile Menu End --> 
            </div>      
        </header>
        <!-- header end -->
        @yield('content')
        <!-- footer start -->
        <footer class="footer_area">
            <div class="footer_top_area text-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="logo">
                                <a href="index.html"><img src="img/logo_footer.png" alt="logo"></a>
                                <p class="center">The merchants represented are not sponsors of gft.to or otherwise affiliated with gft.to.<br/> The logos and other identifying marks attached are trademarks of and owned by each represented company and/or its affiliates.<br/> Please visit each company’s website for additional terms and conditions.</p>
                            </div>
                        </div>
                    </div>`
                </div>
            </div>
            <div class="footer_middle_area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                            <div class="footer_widget">
                                <h4>company</h4>
                                <ul class="footer_menu">
                                    <li><a href="#">about us</a></li>
                                    <li><a href="#">affiliate program</a></li>
                                    <li><a href="#">careers</a></li>
                                    <li><a href="#">press releases</a></li>
                                    <li><a href="#">terms &amp; conditions</a></li>
                                    <li><a href="#">contact us</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 hidden-sm col-xs-12">
                            <div class="footer_widget">
                                <h4>customer service</h4>
                                <ul class="footer_menu">
                                    <li><a href="#">help &amp; faq</a></li>
                                    <li><a href="#">orders &amp; returns</a></li>
                                    <li><a href="#">guarantee</a></li>
                                    <li><a href="#">shipping &amp; policies</a></li>
                                    <li><a href="#">serch &amp; terms</a></li>
                                    <li><a href="#">store locations</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                            <div class="footer_widget">
                                <h4>my account</h4>
                                <ul class="footer_menu">
                                    <li><a href="#">my orders</a></li>
                                    <li><a href="#">my credit slips</a></li>
                                    <li><a href="#">my addresses</a></li>
                                    <li><a href="#">my personal info</a></li>
                                    <li><a href="#">specials</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                            <div class="footer_widget">
                                <h4>newletter</h4>
                                <form action="#">
                                    <input type="text" placeholder="Enter your email address">
                                    <a href="#" class="submit"><i class="fa fa-long-arrow-right"></i></a>
                                </form>
                                <h4 class="follow">follow us on</h4>
                                <ul class="social_icon">
                                    <li class="facebook"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li class="twitter"><a target="_blank" href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li class="dribbble"><a target="_blank" href="#"><i class="fa fa-dribbble"></i></a></li>
                                    <li class="google-plus"><a target="_blank" href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li class="instagram"><a target="_blank" href="#"><i class="fa fa-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- footer end -->
        @include('partials.scripts')
    </body>
</html>
