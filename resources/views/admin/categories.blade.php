@extends('admin.main')
@section('title')
Gft.To | Card Categories
@stop
@section('content')
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-head">
			<div class="page-title">
				<h1>Categories <small>Create, View &amp; Edit Categories</small></h1>
			</div>
		</div>
		<ul class="page-breadcrumb breadcrumb">
			<li>	
				<a href="{{URL::route('admin')}}">Dashboard</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>	
				<a href="{{URL::route('categories')}}">Categories</a>
			</li>
		</ul>
		<div class="row">
			<div class="col-md-12">
					{!! Form::open(array('url' => '/gft-admin/categories', 'class' => 'form-horizontal form-row-seperated')) !!}
						<div class="portlet light">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-tag font-green-sharp"></i>
									<span class="caption-subject font-green-sharp bold uppercase">
									Categories </span>
								</div>
								<div class="actions btn-set">
									<a href="{{URL::route('admin')}}" type="button" name="back" class="btn btn-default btn-circle"><i class="fa fa-angle-left"></i> Back To Dashboard</a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="tabbable">
									<ul class="nav nav-tabs">
										<li class="active">
											<a href="#tab_categories" data-toggle="tab">
											Categories </a>
										</li>
										<li>
											<a href="#tab_addCategory" data-toggle="tab">
											Add New Category
											</a>
										</li>
									</ul>
									<div class="tab-content no-space">
										<div class="tab-pane" id="tab_addCategory">
											<div class="form-body">
												<div class="form-group">
													<label class="col-md-2 control-label">Category Name: <span class="required">
													* </span>
													</label>
													<div class="col-md-10">
														{!!Form::text('name',null, array('class' => 'form-control','required'))!!}
													</div>
												</div>
												<div class="form-group">
													<div class="col-md-12">
														{!!Form::submit('Add Category', array('class' => 'form-control'))!!}
													</div>
												</div>
											</div>
										</div>
										<div class="tab-pane active" id="tab_categories">
											<table class="table table-bordered table-hover">
											<thead>
											<tr role="row" class="heading">
												<th width="25%">
													 Categories
												</th>
												<th width="8%">
													 Action
												</th>
											</tr>
											</thead>
											<tbody>
												@foreach ($categories as $category)
												<tr>
													<td>
														<p>{{ $category->name }}</p>
													</td>
													<td>
														<a href="javascript:;" class="btn-danger btn-sm">
														<i class="fa fa-times"></i> Remove </a>
														<a href={{ url('/gft-admin/categories',$category->id) }} class="btn-primary btn-sm">
														<i class="fa fa-pencil"></i> Edit </a>
													</td>
												</tr>
												@endforeach
											</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					{!! Form::close()!!}
				</div>
		</div>
	</div>
</div>
@stop

