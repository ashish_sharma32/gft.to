<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
	<head>
	@include('admin.partials.meta')
    @include('admin.partials.fonts')
    @include('admin.partials.stylesheet')		 
	</head>
	<body class="page-sidebar-closed-hide-logo page-boxed page-header-fixed" data-gr-c-s-loaded="true">
		<!-- Header -->
		<div class="page-header navbar navbar-fixed-top">
			<div class="page-header-inner container">
				<div class="page-logo">
					<a href="index.html">
					<img src="../admin/assets/admin/layout4/img/logo-light.png" alt="logo" class="logo-default">
					</a>
				</div>
				<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
				</a>
				<div class="page-actions">
					<div class="btn-group">
						<button type="button" class="btn red-haze btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<span class="hidden-sm hidden-xs">Actions&nbsp;</span><i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="javascript:;">
								<i class="icon-docs"></i> New Card </a>
							</li>
							<li>
								<a href="javascript:;">
								<i class="icon-tag"></i> New Category </a>
							</li>
						</ul>
					</div>
				</div>
				<div class="page-top">
					<form class="search-form" action="extra_search.html" method="GET">
						<div class="input-group">
							<input type="text" class="form-control input-sm" placeholder="Search..." name="query">
							<span class="input-group-btn">
							<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
							</span>
						</div>
					</form>
					<div class="top-menu">
						<ul class="nav navbar-nav pull-right">
							<li class="separator hide">
							</li>
							<li class="separator hide">
							</li>
							<li class="dropdown dropdown-user dropdown-dark">
								<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
								<span class="username username-hide-on-mobile">
								Nick </span>
								<img alt="" class="img-circle" src="../admin/assets/admin/layout4/img/avatar9.jpg">
								</a>
								<ul class="dropdown-menu dropdown-menu-default">
									<li>
										<a href="login.html">
										<i class="icon-key"></i> Log Out </a>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- Header End -->
		<div class="clearfix">
		</div><div class="container"><div class="page-container">
			<!-- Sidebar -->
			<div class="page-sidebar-wrapper">
				<div class="page-sidebar navbar-collapse collapse">
					<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
						<li class="start ">
							<a href="{{URL::route('admin')}}">
							<i class="icon-home"></i>
							<span class="title">Dashboard</span>
							</a>
						</li>
						<li>
							<a href="javascript:;">
							<i class="icon-docs"></i>
							<span class="title">View Cards</span>
							</a>
						</li>
						<li>
							<a href="javascript:;">
							<i class="icon-docs"></i>
							<span class="title">Add New Card</span>
							</a>
						</li>
						<li>
							<a href="{{URL::route('categories')}}">
							<i class="icon-tag"></i>
							<span class="title">Categories</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<!--Siderbar End-->
			<!--Content-->
			@yield('content')
		</div>
		<div class="page-footer">
			<div class="page-footer-inner">
				 2016 © GFT.TO | All Rights Reserved
			</div>
		</div>
		</div>

		<div class="scroll-to-top" style="display: none;">
				<i class="icon-arrow-up"></i>
		</div>
		@include('admin.partials.scripts')
		@yield('page-level-scripts')
	</body>
</html>